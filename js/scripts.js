jQuery.noConflict();
jQuery(document).ready(function($){

	$('html').addClass('js');

	$('.mod_menu-main').on('click',function(){
		$('body').toggleClass('mod_menu-main-on');
	});

	$('.mod_search .toggle').on('click',function(){
		$('body').toggleClass('mod_search-on');
	});

	$('.mod_custom-slideshow').each(function(){
		var i = $('.item',this).length;
		$('.item',this).each(function(){
			var bg = $('figure img',this).attr('src');
			$(this).prepend('<div class="bg"><img src="'+bg+'"><img src="'+bg+'"><img src="'+bg+'"></div>');
		});
		if(i>1){
			$(this).append('<div class="pager"/><div class="arrows"><a class="prev"/><a class="next"/></div>');
			$('.items',this).cycle({
				slides: '.item',
				speed: 600,
				timeout: 5000,
				pager: $('.pager',this),
				prev: $('.prev',this),
				next: $('.next',this),
			});
		}
	});

	$('.mod_banners').each(function(){
		$('ul',this).wrap('<div class="slider"/>');
		$('.slider',this).append('<div class="arrows"><a class="prev"/><a class="next"/></div>');
		$('ul',this).cycle({
			fx: 'carousel',
			slides: 'li',
			speed: 300,
			timeout: 9000,
			swipe: true,
			prev: $('.prev',this),
			next: $('.next',this),
		});
	});

	$('.datepicker_example, .datepicker_example_inline').datepicker({ minDate: -30, maxDate: "+30D" });

	// iCheck
	$('input[type="checkbox"],input[type="radio"]').iCheck();

	// selectBoxIt
	$('select:not([multiple]').selectBoxIt();

	// basictable
	$('table.normal').basictable({
		breakpoint: 9999999, // css media query
	});


	$('[data-map-trigger]').mouseover(function(e) {
		var trigger = $(this).data('map-trigger');
		$('[data-map-area="'+trigger+'"]').mouseover();
	}).mouseout(function(e) {
		var trigger = $(this).data('map-trigger');
		$('[data-map-area="'+trigger+'"]').mouseout();
	}).click(function(e) { e.preventDefault(); });



	$('.mod_custom-map-object').each(function(){
		$('.pane',this).addClass('off');
		$('.pane:first-child',this).removeClass('off');
		$('.tabs li:first-child',this).addClass('on');
		$('.tabs li a',this).click(function(e){
			var index = $(this).parents('li').index()+1;
			e.preventDefault();
			$(this).parents('ul').find('li').removeClass('on');
			$(this).parents('li').addClass('on');
			$('.pane').addClass('off');
			$('.pane:nth-child(' + (index++) + ')').removeClass('off');
		});
	});



		  
	$('img[usemap]').rwdImageMaps();

	$('img[usemap]').maphilight({
		fillColor: 'b3000b',
		fillOpacity: 0.3,
		strokeColor: 'b3000b',
		strokeWidth: 3,
		shadowColor: 'ffffff',
		shadow: true,
	});

	// file input style
	$('input[type=file]').each(function(){
		var uploadText = $(this).data('value');
		if (uploadText) {
			var uploadText = uploadText;
		} else {
			var uploadText = 'Choose file';
		}
		var inputClass=$(this).attr('class');
		$(this).wrap('<span class="fileinputs"></span>');
		$(this)
			.parents('.fileinputs')
			.append($('<span class="fakefile"/>')
			.append($('<input class="input-file-text" type="text" />')
			.attr({'id':$(this)
			.attr('id')+'__fake','class':$(this).attr('class')+' input-file-text'}))
			.append('<input type="button" value="'+uploadText+'">'));
		$(this)
			.addClass('type-file')
			.css('opacity',0);
		$(this)
			.bind('change mouseout',function(){$('#'+$(this).attr('id')+'__fake')
			.val($(this).val().replace(/^.+\\([^\\]*)$/,'$1'))
		})
	});

	// placeholder
	if (document.createElement('input').placeholder==undefined){
		$('[placeholder]').focus(function(){
			var input=$(this);
			if(input.val()==input.attr('placeholder')){
				input.val('');
				input.removeClass('placeholder')
			}
		}).blur(function(){
			var input=$(this);
			if(input.val()==''||input.val()==input.attr('placeholder')){
				input.addClass('placeholder');
				input.val(input.attr('placeholder'))
			}
		}).blur()
	}

	// go top
	var go_top = function(){
		var offset = $(window).scrollTop();
		if(offset>100){
			$('#gotop').addClass('on');
		} else {
			$('#gotop').removeClass('on');
		}
	}
	go_top();
	$(window).scroll(function(){
		go_top();
	});
	$('#gotop a').click(function(){ 
		$('html,body').animate({scrollTop:0},600);
		return false;
	});

});